/**************************************************************************** *
 *   nazev projektu: BETON                                                    *
 *   soubor: knihovna.c                                                       *
 *   autor: Vojtech Simsa xsimsa01                                            *
 *                                                                            *
 *   popis: knihovna funkci                                                   *
 *                                                                            *
 ******************************************************************************/
/**
 * @file knihovna.c
 *
 * @brief knihovna funkci
 * @author Vojtech Simsa (xsimsa01)
 */
#include <math.h>
#include <float.h>

/**
 * Struktura obsahujici vysledek a priznak chyby
 */
typedef struct vysledek
{
double result;	/**< Ciselny vysledek */
int error;	/**< Priznak oznacujici chybu */
} Tvysledek;

/**
 * provede operaci scitani a vysledek vrati ve strukture Tvysledek.
 * @param a prvni scitanec.
 * @param b druhy scitanec.
 * @return Vraci strukturu obsahujici soucet a+b.
 */
Tvysledek scitani(double a, double b){
 Tvysledek result={
  .error=0,
  .result=a+b,
  };
 return result;
}

/**
 * provede operaci odcitani a vysledek vrati ve strukture Tvysledek.
 * @param a mensenec.
 * @param b mensitel.
 * @return Vraci strukturu obsahujici rozdil a-b.
 */
Tvysledek odcitani(double a, double b) {
 Tvysledek result={
  .error=0,
  .result=a-b,
  };
 return result;
}

/**
 * provede operaci nasobeni a vysledek vrati ve strukture Tvysledek.
 * @param a prvni cinitel.
 * @param b druhy cinitel.
 * @return Vraci strukturu obsahujici soucin a*b.
 */
Tvysledek nasobeni(double a, double b) {
 Tvysledek result={
  .error=0,
  .result=a*b,
  };
 return result;
}

/**
 * provede operaci deleni a vysledek vrati ve strukture Tvysledek.
 * @param a delenec.
 * @param b delitel.
 * @return Vraci strukturu obsahujici podil a/b.
 */
Tvysledek deleni(double a, double b) {
 Tvysledek result={
  .error=0,
  .result=0,
  };
 if (b==0) {
  result.error=1;
  return result;
  }
 result.result=a/b;
 return result;
}

/**
 * provede operaci scitani a vysledek vrati ve strukture Tvysledek.
 * pokud vysledek neni zobrazitelny v ciselnem rozsahu double je vysledna hodnota .error nastavena na 1.
 * pokud exponent neni prirozene cislo je vysledna hodnota .error nastavena na 1.
 * @param a zaklad mocniny.
 * @param exponent exponent umocneni.
 * @return Vraci strukturu obsahujici hodnotu a^exponent a pripadnou indikaci chyby.
 */
Tvysledek umocneni(double a, double exponent) {
 Tvysledek result={
  .error=0,
  .result=0,
  };
 if (ceil(exponent)!=exponent || exponent<0 || exponent>DBL_MAX_EXP){
  result.error=1;
  return result;
  }
 result.result=pow(a,exponent);
 return result;
}

/**
 * vypocita faktorial a vysledek vrati ve strukture Tvysledek.
 * pokud vysledek neni zobrazitelny v ciselnem rozsahu double je vysledna hodnota .error nastavena na 1.
 * pokud zaklad faktorialu neni prirozene cislo je vysledna hodnota .error nastavena na 1.
 * @param a zaklad faktorialu.
 * @return Vraci strukturu obsahujici hodnotu a! a pripadnou indikaci chyby.
 */
Tvysledek faktorial(double a){
 int x=1;
 Tvysledek result={
  .error=0,
  .result=1,
  };
 if (ceil(a)!=a || a<0){
  result.error=1;
  return result;
  }
 while (x<=a){
 if (result.result>(DBL_MAX/x)) {
  result.error=1;
  return result;
  }
 result.result=result.result*x;
 x++;
 }
 return result;
}

/**
 * vypocita prirozeny logaritmus a vysledek vrati ve strukture Tvysledek.
 * pokud je numerus logaritmu zaporny je vysledna hodnota .error nastavena na 1.
 * @param a numerus logaritmu.
 * @return Vraci strukturu obsahujici hodnotu ln(a) a pripadnou indikaci chyby.
 */
Tvysledek logaritmus(double a) {
 Tvysledek result={
  .error=0,
  .result=0,
  };
 if (a<=0){
  result.error=1;
  return result;
 }
 result.result=log(a);
 return result;
}
/*** konec souboru knihovna.c ***/
