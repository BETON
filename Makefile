
CC=gcc
CFLAGS=-std=c99 -g -pedantic -Wall -lm -export-dynamic `pkg-config --cflags --libs gtk+-2.0`

OBJFILES = testy.o knihovna.o
.PHONY: dep clean doc pack test

BIN=KAL_BETON

#%.o : %.c
#	$(CC) $(CFLAGS) -c $<

all: $(BIN)

debug: CFLAGS+= -g3
debug: all

dep:
	$(CC) -MM *.c > .depend

test: $(OBJFILES)
	$(CC) $(CFLAGS) $(OBJFILES) -o $@
	@./test

#knihovna: knihovna.o
#	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -f test *.o $(BIN)

doc:
	doxygen Doxyfile

$(BIN): main.c kal.glade knihovna.o
#	gtk-builder-convert IVS_example.glade IVS_example.xml
	$(CC) $(CFLAGS) -o $@ main.c knihovna.o

