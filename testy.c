/**************************************************************************** *
 *   nazev projektu: BETON                                                    *
 *   soubor: testy.c                                                          *
 *   autor: Vojtech Simsa xsimsa01                                            *
 *                                                                            *
 *   popis: sada automatickych testu nad knihovnou knihovna.c                 *
 *                                                                            *
 ******************************************************************************/
/**
 * @file testy.c
 *
 * @brief automaticke testy
 * @author Vojtech Simsa (xsimsa01)
 */
#include "knihovna.h"
#include <assert.h>
#include <stdio.h>
/**
 * test scitani
 */
void test_scitani(void){
 Tvysledek testsum1=scitani(56, 98.43);
 assert(testsum1.result==154.43);
 assert(testsum1.error==0);
}

/**
 * test odcitani
 */
void test_odcitani(void){
 Tvysledek testsub1=odcitani(41,18.5);
 assert(testsub1.result==22.5);
 assert(testsub1.error==0);
}

/**
 * test nasobeni
 */
void test_nasobeni(void){
 Tvysledek testmult1=nasobeni(2,15.5);
 assert(testmult1.result==31);
 assert(testmult1.error==0);
}

/**
 * test deleni
 */
void test_deleni(void){
 Tvysledek testdiv1=deleni(31,2);
 assert(testdiv1.result==15.5);
 assert(testdiv1.error==0);
 Tvysledek testdiv2=deleni(31,0);
 assert(testdiv2.error==1);
}

/**
 * test umocnovani
 */
void test_umocneni(void){
 Tvysledek testpow1=umocneni(2,3);
 assert(testpow1.result==8);
 assert(testpow1.error==0);
 Tvysledek testpow2=umocneni(2,3.5);
 assert(testpow2.error==1);
 Tvysledek testpow3=umocneni(2,-3);
 assert(testpow3.error==1);
 Tvysledek testpow4=umocneni(2,-3.5);
 assert(testpow4.error==1);
 Tvysledek testpow5=umocneni(2,0);
 assert(testpow5.result==1);
 assert(testpow5.error==0);
}

/**
 * test faktorial
 */
void test_faktorial(void){
 Tvysledek testfact1=faktorial(8);
 assert(testfact1.result==40320);
 assert(testfact1.error==0);
 Tvysledek testfact2=faktorial(8.5);
 assert(testfact2.error==1);
 Tvysledek testfact3=faktorial(-8.5);
 assert(testfact3.error==1);
 Tvysledek testfact4=faktorial(-6);
 assert(testfact4.error==1);
 Tvysledek testfact5=faktorial(0);
 assert(testfact5.result==1);
 assert(testfact5.error==0);
}

/**
 * test logaritmu
 */
void test_logaritmus(void){
 Tvysledek testlog1=logaritmus(56);
 assert(testlog1.result==log(56));
 assert(testlog1.error==0);
 Tvysledek testlog2=logaritmus(-56);
 assert(testlog2.error==1);
}

/**
 * primarni funkce spousteni testu
 */
int main(){
test_scitani();
test_odcitani();
test_nasobeni();
test_deleni();
test_umocneni();
test_faktorial();
test_logaritmus();
return 0;
}
/*** konec souboru testy.c ***/
