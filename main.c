/**************************************************************************** *
 *   nazev projektu: BETON                                                    *
 *   soubor: main.c                                                           *
 *   autor:            Michal Rysavy xrysav17                                 *
 *   kultirizace kodu: Jakub Jelen   xjelen06                                 *
 *                                                                            *
 *   popis: hlavni logika aplikace                                            *
 *                                                                            *
 ******************************************************************************/
/**
 * @file main.c 
 *
 * @author Michal Rysavy (korektura: Jakub Jelen)
 * @brief Hlavni logika kalkulacky a volani GTK+
 */
#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "knihovna.h"

/** Jmeno souboru s XML formatem GUI */
#define BUILDER_XML_FILE "kal.glade"

/**
 * struktura s ukazateli na jednotlive widgety v aplikaci
 */
typedef struct
{
	GtkWidget	*window;	/**< Okno aplikace */
	GtkWidget	*plus;		/**< Tlacitko + */
	GtkWidget	*minus;		/**< Tlacitko - */
	GtkWidget	*nasobeni;	/**< Tlacitko * */
	GtkWidget	*deleni;	/**< Tlacitko : */
	GtkWidget	*umocni;	/**< Tlacitko X^n */
	GtkWidget	*button5;	/**< Tlacitko log */
	GtkWidget	*button6;	/**< Tlacitko n! */
	GtkWidget	*CE;		/**< Tlacitko Resetu kalkula4ky */
	GtkWidget	*n1;		/**< Tlacitko 1 */
	GtkWidget	*n2;		/**< Tlacitko 2 */
	GtkWidget	*n3;		/**< Tlacitko 3 */
	GtkWidget	*n4;		/**< Tlacitko 4 */
	GtkWidget	*n5;		/**< Tlacitko 5 */
	GtkWidget	*n6;		/**< Tlacitko 6 */
	GtkWidget	*n7;		/**< Tlacitko 7 */
	GtkWidget	*n8;		/**< Tlacitko 8 */
	GtkWidget	*n9;		/**< Tlacitko 9 */
	GtkWidget	*n0;		/**< Tlacitko 0 */
	GtkWidget	*desetina;		/**< Tlacitko , */
	GtkWidget	*spocitej;		/**< Tlacitko = */
	GtkLabel	*label1;		/**< Pole pro zobrazeni vysledku */
} App;

/** deklarace globalnich promenych pro vysledky kalkulacky
 */
	Tvysledek hodnota1, hodnota2;

/**
 * Enumerace operaci kalkulacky pro promennou operace
 */			
enum OP{
	NONE = 0, // nic / nebylo zadano
	PLUS,     // scitani
	MINUS,    // odcitani
	NASOB,    // nasobeni
	DEL,      // deleni
	MOCNI,    // mocnina
	LOG,      // logaritmus
	FACT,     // faktorial
	VYPOCTENO // vypocteno
} operace;					/**< Priznak operace pro stav kalkulacky */

int desetina=0; /**< pri hodnote 1 se už tvori desetina cisla */
int cislic=0;	/**< Pocet zadanych cislic */
int omezeni=8;  /**< omezeni kolik maximalne muze uzivatel zadat cislic na jedno cislo */

/**
 * Pomocna promenna pro vypis
 */
char* x;

/**
 * @brief Funkce nastavujici operaci do globalni promenne, spolecne s resenim drobnosti pri prechodu na dalsi vstupni cislo.
 * 
 * @param op Konstanta symbolizujici opeaci z pole OP
 */
void setOperation( int op ){
	if( (operace == NONE) || (operace == VYPOCTENO)){ //zadano jen jedno cislo
		operace = op;
		cislic = 0;
		desetina = 0;
	} else{ // Operace jiz vybrana
		//zde by se mela provest operace, pokud jsou jiz dve cisla
	}	
}


/**
 * handler, ktery je v glade prirazen stisknuti tlacitka +
 */
void plus_clicked (GtkObject *object, App *app){
	setOperation(PLUS);
}

/**
 * handler, ktery je v glade prirazen stisknuti tlacitka -
 */
void minus_clicked (GtkObject *object, App *app){
	setOperation(MINUS);
}

/**
 * handler, ktery je v glade prirazen stisknuti tlacitka *
 */
void nasobeni_clicked (GtkObject *object, App *app){
	setOperation(NASOB);
}

/**
 * handler, ktery je v glade prirazen stisknuti tlacitka :
 */
void deleni_clicked (GtkObject *object, App *app){
	setOperation(DEL);
}

/**
 * handler, ktery je v glade prirazen stisknuti tlacitka umocneni
 */
void umocni_clicked (GtkObject *object, App *app){
	setOperation(MOCNI);
}

/**
 * handler, ktery je v glade prirazen stisknuti tlacitka log a rovnou vypocita
 */
void on_button5_clicked (GtkObject *object, App *app)
{
	hodnota1=logaritmus(hodnota1.result);
			if (hodnota1.error!=0){
	gtk_label_set_text(app->label1,"ERROR");
	operace=0;
				desetina=0;
				hodnota1.result=0;
				hodnota2.result=0;
				cislic=0;
			}
			else{
	char* x = malloc(sizeof(char)*15);
				sprintf(x,"%.5f",hodnota1.result);
				gtk_label_set_text(app->label1,x);
				hodnota2.result=0;
	operace=0; //uz nebude mozne zadavat hodnotu 1
	cislic=8;
	desetina=1;
	free(x);
			}
}

/**
 * handler, ktery je v glade prirazen stisknuti tlacitka faktorial a rovnou vypocita
 */
void on_button6_clicked (GtkObject *object, App *app)
{
	hodnota1=faktorial(hodnota1.result);
			if (hodnota1.error!=0){
	gtk_label_set_text(app->label1,"ERROR");
	operace=0;
				desetina=0;
				hodnota1.result=0;
				hodnota2.result=0;
				cislic=0;
			}
			else{
		char* x = malloc(sizeof(char)*15);
				sprintf(x,"%.5f",hodnota1.result);
				gtk_label_set_text(app->label1,x);
				hodnota2.result=0;
	operace=0; //uz nebude mozne zadavat hodnotu 1
	cislic=8;
	desetina=1;
	free(x);
			}
}
/**
 * handler, ktery vynuluje kalkulacku
 */
void CE_clicked (GtkObject *object, App *app)
{
	operace=0;
	desetina=0;
	hodnota1.result=0;
	hodnota1.error=0;
	hodnota2.result=0;
	hodnota2.error=0;
	cislic=0;
	gtk_label_set_text(app->label1,"Kalkulacka BETON");
}


/**
 * @brief Prida cislo zadane parametrem podle stavu kalkulacky do spravneho cisla a vypise aktualni stav
 * @param app	 GTK Aplikace
 * @param number Jednociferne cislo k zadani
 */
void addNumber(App* app, double number){
	if (cislic < omezeni){
		cislic++; 
		if (operace == 0){
			if (desetina == 0){
				hodnota1.result = hodnota1.result * 10 + number;
			} else{
				hodnota1.result = hodnota1.result + (number / pow(10, desetina));
				desetina++;
			}
			sprintf(x,"%.5f",hodnota1.result);
			gtk_label_set_text(app->label1,x);
		} else {
			if (desetina == 0){
				hodnota2.result = hodnota2.result * 10 + number;
			} else {
				hodnota2.result = hodnota2.result + (number/pow(10,desetina));
				desetina++;
			}
			sprintf(x,"%.5f",hodnota2.result);
			gtk_label_set_text(app->label1,x);
		}
	}
}

/**
 * Handler tlacitka s cislem 1
 */
void n1_clicked (GtkObject *object, App *app){
	addNumber(app, 1);
}

/**
 * Handler tlacitka s cislem 2
 */
void n2_clicked (GtkObject *object, App *app){
	addNumber(app, 2);
}

/**
 * Handler tlacitka s cislem 3
 */
void n3_clicked (GtkObject *object, App *app){
	addNumber(app, 3);
}

/**
 * Handler tlacitka s cislem 4
 */
void n4_clicked (GtkObject *object, App *app){
	addNumber(app, 4);
}

/**
 * Handler tlacitka s cislem 5
 */
void n5_clicked (GtkObject *object, App *app){
	addNumber(app, 5);
}

/**
 * Handler tlacitka s cislem 6
 */
void n6_clicked (GtkObject *object, App *app){
	addNumber(app, 6);
}

/**
 * Handler tlacitka s cislem 7
 */
void n7_clicked (GtkObject *object, App *app){
	addNumber(app, 7);
}

/**
 * Handler tlacitka s cislem 8
 */
void n8_clicked (GtkObject *object, App *app){
	addNumber(app, 8);
}

/**
 * Handler tlacitka s cislem 9
 */
void n9_clicked (GtkObject *object, App *app){
	addNumber(app, 9);
}

/**
 * Handler tlacitka s cislem 0
 */
void n0_clicked (GtkObject *object, App *app){
	addNumber(app, 0);
}

/**
 * Nastavi desetinnou carku a pristi cislice jsou desetinna
 */
void desetina_clicked (GtkObject *object, App *app)
{
	if (desetina==0){
		desetina=1;
		//mozna jeste neco
	}
	else{ //vypsani chyby pokud se vickrat klikne na desetinu
		
	}
}
/**
 * Podle typu operace zavola prislusnou funkci z nasi uchvatne knihovny
 */
void spocitej_clicked (GtkObject *object, App *app)
{
	switch (operace){
		case PLUS:
			hodnota1 = scitani(hodnota1.result, hodnota2.result);
			sprintf(x,"%.5f",hodnota1.result);
			gtk_label_set_text(app->label1,x);
			break;

	 	case MINUS:
			hodnota1 = odcitani(hodnota1.result, hodnota2.result);
			sprintf(x,"%.5f",hodnota1.result);
			gtk_label_set_text(app->label1,x);
			break;

		 case NASOB:
			hodnota1 = nasobeni(hodnota1.result, hodnota2.result);
			sprintf(x,"%.5f",hodnota1.result);
			gtk_label_set_text(app->label1,x);
			break;

		case DEL:
			hodnota1 = deleni(hodnota1.result, hodnota2.result);
			if (hodnota1.error != 0){
				gtk_label_set_text(app->label1,"ERROR");
				desetina=0;
				hodnota1.result = 0;
				cislic=0;
			}
			else{
				sprintf(x,"%.5f",hodnota1.result);
				gtk_label_set_text(app->label1,x);
			}
			break;
		
		case MOCNI:
			hodnota1 = umocneni(hodnota1.result, hodnota2.result);
			sprintf(x,"%.5f", hodnota1.result);
			gtk_label_set_text(app->label1, x);
			break;
		default:
			// ...
			break;
			 
	}
	hodnota2.result = 0;
	operace = VYPOCTENO;
}


/**
 * ukonci hlavni smycku gtk pri zavreni okna
 */
void on_window_destroy (GtkObject *object, gpointer user_data)
{
	free(x);
	gtk_main_quit();
}
	 
/**
 * @brief Main funkce programu, nastavi globalni promenne, GUI a vse spusti
 * Vynuluje globalni promenne a nastavi GTK a vytovori GUI.
 */
int main (int argc, char *argv[])
{ 
	// Vychozi nastaveni kalkulacky
	hodnota1.result=0;
	hodnota1.error=0;	
	hodnota2.result=0;
	hodnota2.error=0;
	App *app;
	GtkBuilder			*builder; 

	// retezec pro vypis cisel
	x = malloc(sizeof(char)*15);

	// alokuje pamet pro app	
	app = g_slice_new(App);

		gtk_init (&argc, &argv);
	
	// nacte GUI z XML
		builder = gtk_builder_new ();
		gtk_builder_add_from_file (builder, BUILDER_XML_FILE, NULL);
	
	
	// priradi do struktury jednotlive widgety
	app->window = GTK_WIDGET (gtk_builder_get_object (builder, "window1"));
	app->plus = GTK_WIDGET (gtk_builder_get_object (builder, "plus"));
	app->minus = GTK_WIDGET (gtk_builder_get_object (builder, "minus"));
	app->nasobeni = GTK_WIDGET (gtk_builder_get_object (builder, "nasobeni"));
	app->deleni = GTK_WIDGET (gtk_builder_get_object (builder, "deleni"));
	app->umocni = GTK_WIDGET (gtk_builder_get_object (builder, "umocni"));
	app->button5 = GTK_WIDGET (gtk_builder_get_object (builder, "button5"));
	app->button6 = GTK_WIDGET (gtk_builder_get_object (builder, "button6"));
	app->CE = GTK_WIDGET (gtk_builder_get_object (builder, "CE"));
	app->n1 = GTK_WIDGET (gtk_builder_get_object (builder, "n1"));
	app->n2 = GTK_WIDGET (gtk_builder_get_object (builder, "n2"));
	app->n3 = GTK_WIDGET (gtk_builder_get_object (builder, "n3"));
	app->n4 = GTK_WIDGET (gtk_builder_get_object (builder, "n4"));
	app->n5 = GTK_WIDGET (gtk_builder_get_object (builder, "n5"));
	app->n6 = GTK_WIDGET (gtk_builder_get_object (builder, "n6"));
	app->n7 = GTK_WIDGET (gtk_builder_get_object (builder, "n7"));
	app->n8 = GTK_WIDGET (gtk_builder_get_object (builder, "n8"));
	app->n9 = GTK_WIDGET (gtk_builder_get_object (builder, "n9"));
	app->n0 = GTK_WIDGET (gtk_builder_get_object (builder, "n0"));
	app->desetina = GTK_WIDGET (gtk_builder_get_object (builder, "desetina"));
	app->spocitej = GTK_WIDGET (gtk_builder_get_object (builder, "spocitej"));
	app->label1 = GTK_LABEL (gtk_builder_get_object (builder, "label1"));

	gtk_builder_connect_signals (builder, app);
	g_object_unref (G_OBJECT (builder));
			
	gtk_widget_show (app->window);								
	gtk_widget_set_size_request(app->window, 250, 300);
	//hlavni smycka gtk
	gtk_main ();
	
	free(x);
	return 0;
}
