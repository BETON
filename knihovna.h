#include <math.h>
#include <float.h>

typedef struct vysledek
{
	double result;
	int error;
} Tvysledek;

Tvysledek scitani(double a, double);
Tvysledek odcitani(double a, double b);
Tvysledek nasobeni(double a, double b);
Tvysledek deleni(double a, double b);
Tvysledek umocneni(double a, double exponent);
Tvysledek faktorial(double a);
Tvysledek logaritmus(double a);

/*** konec souboru knihovna.h ***/
